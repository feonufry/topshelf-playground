﻿using System;
using System.Collections.Specialized;
using Common.Logging;
using Quartz;
using Quartz.Impl;
using Reset.WinServiceBase.Config;

namespace Reset.WinServiceBase
{
	public class QuartzJobExecutor
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(QuartzJobExecutor));

		private readonly IScheduler _scheduler;

		public QuartzJobExecutor(IQuartzExecutorOptions options)
		{
			var properties = new NameValueCollection();
			properties["quartz.scheduler.instanceName"] = string.Format("{0}Scheduler", GetType().Name);
			if (options.ThreadsCount > 0)
			{
				properties["quartz.threadPool.threadCount"] = options.ThreadsCount.ToString();
			}
			if (options.MisfireThresholdMiliseconds > 0)
			{
				properties["quartz.jobStore.misfireThreshold"] = options.MisfireThresholdMiliseconds.ToString("D");
			}

			_scheduler = new StdSchedulerFactory(properties).GetScheduler();
		}

		public void Start()
		{
			ManageScheduler(s => s.Start(), "started");
		}

		public void Stop()
		{
			ManageScheduler(s => s.Shutdown(true), "stopped");
		}

		public void Pause()
		{
			ManageScheduler(s => s.Standby(), "paused");
		}

		public void Continue()
		{
			ManageScheduler(s => s.Start(), "continued");
		}

		private void ManageScheduler(Action<IScheduler> action, string title)
		{
			try
			{
				action.Invoke(_scheduler);
			}
			catch (SchedulerException ex)
			{
				Log.Error(string.Format("Quartz Scheduler couldn't be {0}.", title), ex);
			}
		}
	}
}