﻿
using Reset.WinServiceBase.Config;
using Topshelf;
using Topshelf.HostConfigurators;

namespace Reset.WinServiceBase
{
	public abstract class BaseJobsHostService
	{
		public void Register(HostConfigurator config)
		{
			config.Service<QuartzJobExecutor>(s =>
			{
				s.ConstructUsing(CreateJobExecutor);
				s.WhenStarted(tc => tc.Start());
				s.WhenStopped(tc => tc.Stop());
				s.WhenPaused(tc => tc.Pause());
				s.WhenContinued(tc => tc.Continue());
			});

			ConfigureService(config);
		}

		protected virtual QuartzJobExecutor CreateJobExecutor()
		{
			var options = new AppConfigQuartzExecutorOptions();
			return new QuartzJobExecutor(options);
		}

		protected virtual void ConfigureJobs(IJobsConfigurator config)
		{}

		protected virtual void ConfigureService(HostConfigurator config)
		{
			config.SetServiceName(GetType().Name);
			config.SetDisplayName(GetType().FullName);
			config.SetDescription(string.Empty);

			config.EnablePauseAndContinue();
			config.EnableShutdown();

			config.RunAsLocalSystem();
			config.StartAutomatically();
		}
	}
}