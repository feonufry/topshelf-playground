﻿namespace Reset.WinServiceBase.Config
{
	public enum PeriodicMisfire
	{
		/// <summary>
		/// This instruction should typically only be used for
		/// 'one-shot' (non-repeating) Triggers. If it is used on a trigger with a
		/// repeat count &gt; 0 then it is equivalent to the 
		/// instruction <see cref="PeriodicMisfire.NowWithRemainingCount"/>
		/// </summary>
		FireNow,

		/// <summary>
		/// Instructs the Scheduler that the Trigger will never be evaluated for a misfire situation,
		/// and that the scheduler will simply try to fire it as soon as it can, and then 
		/// update the Trigger as if it had fired at the proper time.
		/// </summary>
		Ignore,

		NowWithExistingCount,
		NowWithRemainingCount,
		NextWithExistingCount,
		NextWithRemainingCount,
	}
}