﻿using System.Configuration;

namespace Reset.WinServiceBase.Config
{
	public class AppConfigQuartzExecutorOptions : IQuartzExecutorOptions
	{
		public int ThreadsCount
		{
			get { return ReadInt32("jobExecutor.threads", -1); }
		}

		public int MisfireThresholdMiliseconds
		{
			get { return ReadInt32("jobExecutor.misfireAfter", -1); }
		}

		private int ReadInt32(string key, int defaultValue)
		{
			int result;
			var value = ConfigurationManager.AppSettings[key];
			return value != null && !int.TryParse(value, out result)
				? result
				: defaultValue;
		}
	}
}