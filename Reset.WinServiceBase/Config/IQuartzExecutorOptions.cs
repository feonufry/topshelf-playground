﻿namespace Reset.WinServiceBase.Config
{
	public interface IQuartzExecutorOptions
	{
		int ThreadsCount { get; } 
		int MisfireThresholdMiliseconds { get; } 
	}
}