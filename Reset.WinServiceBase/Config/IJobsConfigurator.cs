﻿using System;
using Quartz;

namespace Reset.WinServiceBase.Config
{
	public interface IJobsConfigurator
	{
		void Cron<TJob>(string cron, CronMisfire misfire)
			where TJob : IJob;

		void PeriodicFinite<TJob>(TimeSpan interval, int repeats, PeriodicMisfire misfire)
			where TJob : IJob;

		void Periodic<TJob>(TimeSpan interval, PeriodicMisfire misfire)
			where TJob : IJob;
	}
}