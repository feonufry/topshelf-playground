﻿using System;
using System.Collections.Generic;
using Quartz;

namespace Reset.WinServiceBase.Config
{
	public class JobsConfigurator : IJobsConfigurator
	{
		private class JobWithTrigger
		{
			private readonly IJobDetail _jobDetail;
			private readonly ITrigger _trigger;

			public IJobDetail JobDetail
			{
				get { return _jobDetail; }
			}

			public ITrigger Trigger
			{
				get { return _trigger; }
			}

			public JobWithTrigger(IJobDetail jobDetail, ITrigger trigger)
			{
				_jobDetail = jobDetail;
				_trigger = trigger;
			}
		}

		private readonly string _group;
		private readonly List<JobWithTrigger> _jobsTriggers = new List<JobWithTrigger>(); 

		public JobsConfigurator(string @group)
		{
			_group = @group;
		}

		public void Cron<TJob>(string cron, CronMisfire misfire) where TJob : IJob
		{
			var jobName = typeof(TJob).FullName;

			var jobDetails = CreateJobDetail<TJob>(jobName);
			var trigger = CreateCronTrigger(jobName, cron, misfire);

			_jobsTriggers.Add(new JobWithTrigger(jobDetails, trigger));
		}

		public void PeriodicFinite<TJob>(TimeSpan interval, int repeats, PeriodicMisfire misfire)
			where TJob : IJob
		{
			var jobName = typeof (TJob).FullName;

			var jobDetails = CreateJobDetail<TJob>(jobName);
			var trigger = CreateSimpleTrigger(jobName, interval, repeats, misfire);

			_jobsTriggers.Add(new JobWithTrigger(jobDetails, trigger));
		}

		public void Periodic<TJob>(TimeSpan interval, PeriodicMisfire misfire)
			where TJob : IJob
		{
			var jobName = typeof(TJob).FullName;

			var jobDetails = CreateJobDetail<TJob>(jobName);
			var trigger = CreateSimpleTrigger(jobName, interval, -1 /*forever*/, misfire);

			_jobsTriggers.Add(new JobWithTrigger(jobDetails, trigger));
		}

		private IJobDetail CreateJobDetail<TJob>(string jobName)
			where TJob : IJob
		{
			return JobBuilder.Create<TJob>()
				.WithIdentity(jobName, _group)
				.Build();
		}

		private ITrigger CreateSimpleTrigger(string jobName, TimeSpan interval, int repeats, PeriodicMisfire misfire)
		{
			return TriggerBuilder.Create()
				.WithIdentity(jobName + ".Trigger", _group)
				.StartNow()
				.WithSimpleSchedule(x =>
				{
					x.WithInterval(interval);
					if (repeats <= 0)
					{
						x.RepeatForever();
					}
					else
					{
						x.WithRepeatCount(repeats);
					}
					switch (misfire)
					{
						case PeriodicMisfire.NowWithExistingCount:
							x.WithMisfireHandlingInstructionNowWithExistingCount();
							break;
						case PeriodicMisfire.NowWithRemainingCount:
							x.WithMisfireHandlingInstructionNowWithRemainingCount();
							break;
						case PeriodicMisfire.NextWithExistingCount:
							x.WithMisfireHandlingInstructionNextWithExistingCount();
							break;
						case PeriodicMisfire.NextWithRemainingCount:
							x.WithMisfireHandlingInstructionNextWithRemainingCount();
							break;
						case PeriodicMisfire.FireNow:
							x.WithMisfireHandlingInstructionFireNow();
							break;
						case PeriodicMisfire.Ignore:
							x.WithMisfireHandlingInstructionIgnoreMisfires();
							break;
					}
				})
				.Build();
		}

		private ITrigger CreateCronTrigger(string jobName, string cron, CronMisfire misfire)
		{
			return TriggerBuilder.Create()
				.WithIdentity(jobName + ".Trigger", _group)
				.StartNow()
				.WithCronSchedule(cron, x =>
				{
					switch (misfire)
					{
						case CronMisfire.DoNothing:
							x.WithMisfireHandlingInstructionDoNothing();
							break;
						case CronMisfire.Ignore:
							x.WithMisfireHandlingInstructionIgnoreMisfires();
							break;
						case CronMisfire.FireAndProceed:
							x.WithMisfireHandlingInstructionFireAndProceed();
							break;
					}
				})
				.Build();
		}
	}
}