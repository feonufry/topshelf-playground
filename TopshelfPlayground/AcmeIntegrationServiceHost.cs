﻿
using System;
using Reset.WinServiceBase;
using Reset.WinServiceBase.Config;
using Topshelf;
using Topshelf.HostConfigurators;
using TopshelfPlayground.Jobs;

namespace TopshelfPlayground
{
	public class AcmeIntegrationServiceHost : BaseJobsHostService
	{
		protected override void ConfigureService(HostConfigurator config)
		{
			config.SetServiceName("AcmeService");
			config.SetDisplayName("ACME Integration");
			config.SetDescription("Lorem ipsum sid amet dolor");

			config.EnablePauseAndContinue();
			config.EnableShutdown();

			config.RunAsLocalSystem();
			config.StartAutomatically();

			config.DependsOnEventLog();
		}

		protected override void ConfigureJobs(IJobsConfigurator config)
		{
			config.Periodic<FooJob>(TimeSpan.FromSeconds(7), PeriodicMisfire.NextWithRemainingCount);
			config.Cron<BarJob>("0/7 * * * * ?", CronMisfire.DoNothing);
		}
	}
}