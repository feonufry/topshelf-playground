﻿using log4net.Config;
using Topshelf;

namespace TopshelfPlayground
{
    class Program
    {
        static void Main(string[] args)
        {
			XmlConfigurator.Configure();

			HostFactory.Run(x =>
			{
				x.UseLog4Net();
				new AcmeIntegrationServiceHost().Register(x);
			});
		}
	}
}
