﻿using System.Collections.Specialized;
using log4net;
using Quartz;
using Quartz.Impl;
using Topshelf;
using TopshelfPlayground.Jobs;

namespace TopshelfPlayground
{
	public class BackgroundJobsService
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof (BackgroundJobsService));

		private readonly IScheduler _scheduler;

		public BackgroundJobsService()
		{
			var config = new NameValueCollection();
			config["quartz.scheduler.instanceName"] = "MAIN-SCHEDULER";
			config["quartz.threadPool.threadCount"] = "3";
			config["quartz.jobStore.misfireThreshold"] = "2000";

			_scheduler = new StdSchedulerFactory(config).GetScheduler();

			var fooJob = JobBuilder.Create<FooJob>()
				.WithIdentity("foo-job", "jobs-group")
				.Build();
			var barJob = JobBuilder.Create<BarJob>()
				.WithIdentity("bar-job", "jobs-group")
				.Build();

			var fooTrigger = TriggerBuilder.Create()
				.WithIdentity("foo-job-trigger", "jobs-group")
				.StartNow()
				.WithSimpleSchedule(x => x
					.WithIntervalInSeconds(7)
					.RepeatForever()
					.WithMisfireHandlingInstructionNextWithRemainingCount()
					)
				.Build();
			var barTrigger = TriggerBuilder.Create()
				.WithIdentity("bar-job-trigger", "jobs-group")
				.StartNow()
				.WithCronSchedule("0/7 * * * * ?", x => x.WithMisfireHandlingInstructionDoNothing())
				.Build();

			_scheduler.ScheduleJob(fooJob, fooTrigger);
			_scheduler.ScheduleJob(barJob, barTrigger);
		}

		public void Start()
		{
			try
			{
				_scheduler.Start();
			}
			catch (SchedulerException ex)
 			{
				Log.Error("Quartz couldn't start.", ex);
			}
		}

		public void Stop()
		{
			try
			{
				_scheduler.Shutdown();
			}
			catch (SchedulerException ex)
			{
				Log.Error("Quartz couldn't stop.", ex);
			}
		}
	}
}