﻿using System;
using System.Threading;
using log4net;
using Quartz;

namespace TopshelfPlayground.Jobs
{
	public class FooJob : IJob
	{
		private static readonly ILog Log = LogManager.GetLogger("JOBS");

		public void Execute(IJobExecutionContext context)
		{
			var id = Guid.NewGuid();
			Log.Info($"[{DateTime.Now:HH:mm:ss}] Foo Started: {id:N}");
			Thread.Sleep(10000);
			Log.Info($"[{DateTime.Now:HH:mm:ss}] Foo Completed: {id:N}");
		}
	}
}