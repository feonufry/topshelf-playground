﻿using System;
using System.Threading;
using log4net;
using Quartz;

namespace TopshelfPlayground.Jobs
{
	public class BarJob : IJob
	{
		private static readonly ILog Log = LogManager.GetLogger("JOBS");

		public void Execute(IJobExecutionContext context)
		{
			var id = Guid.NewGuid();
			Log.Info($"[{DateTime.Now:HH:mm:ss}] Bar Started: {id:N}");
			Thread.Sleep(10000);
			Log.Info($"[{DateTime.Now:HH:mm:ss}] Bar Completed: {id:N}");
		}
	}
}